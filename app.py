from enum import unique
from flask import Flask , redirect, session,render_template,request,Response
from flask.helpers import url_for
from flask_wtf import FlaskForm, form
from werkzeug.exceptions import default_exceptions
from wtforms import StringField , SubmitField,TextAreaField
from flask_sqlalchemy import SQLAlchemy
from datetime import  datetime
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///try.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY']='sjkasjaksjasjaksj'
db = SQLAlchemy(app)

class Todo(db.Model):
    srno = db.Column(db.Integer,primary_key =True)
    name = db.Column(db.String(60),nullable=False ,unique=True)
    Roll_num = db.Column(db.String,nullable=True)
    Email = db.Column(db.String,nullable=False)
    phone = db.Column(db.String(100),nullable=False)
    img = db.Column(db.Text,nullable=True)
    img_name = db.Column(db.Text,nullable=True)
    mimetype = db.Column(db.Text,nullable=True)
    dat_ = db.Column(db.DateTime,default=datetime.utcnow)

    def __repr__(self) -> str:
        return f'{self.srno} - {self.name}'

@app.route('/',methods=['POST',"GET"])
def Home():
    if request.method == "POST":
        name = request.form.get('Name')
        roll_num = request.form.get('roln')
        Email_ = request.form.get('email')
        phone = request.form.get('num')
        pic = request.files['data']
        if not pic:
            return "NO PIC ",404
        filename = secure_filename(pic.filename)
        mime = pic.mimetype
        gg = Todo(name=name,Roll_num=roll_num,Email=Email_,phone=phone,img=pic.read(),img_name=filename,mimetype=mime)
        db.session.add(gg)
        db.session.commit()
    return render_template('home.html')

@app.route('/g')
def GG():
    all_data = Todo.query.all()
    return render_template('main.html',all_data=all_data)
@app.route('/del/<int:srno>')
def delete(srno):
    del_ = Todo.query.filter_by(srno=srno).first()
    db.session.delete(del_)
    db.session.commit()
    return redirect(url_for('GG'))
@app.route('/pri/<int:srno>')
def prin(srno):
    img = Todo.query.filter_by(srno=srno).first()
    if not img:
        return 'No IMG'
    return Response(img.img,mimetype=img.mimetype)

@app.route('/update/<int:srno>')
def update(srno):
    Update = Todo.query.filter_by(srno=srno).first()
    return render_template('gg.html',Update=Update)

if __name__ == '__main__':
    app.run(debug=True,port=2000)
