# Flask Sqlalchemy 

Using Flask Sqlalchemy  A simple  html form students information 

### requirements module 
1. [pip3 install Flask](https://pypi.org/project/Flask/)
2. [pip3 install Flask-SQLAlchemy](https://pypi.org/project/Flask-SQLAlchemy/)
3. [pip3 install Flask-WTF](https://pypi.org/project/Flask-WTF/)


### Flask 
Flask is a web framework, it’s a Python module that lets you develop web applications easily. It’s has a small and easy-to-extend core: it’s a microframework that doesn’t include an ORM (Object Relational Manager) or such features.
It does have many cool features like url routing, template engine. It is a WSGI web app framework.

### What is a CRUD application?:
_A web application that deals with Create/Retrieve/Update or Delete operations is known as a CRUD application. A typical example of a CRUD application is a Blog Website._


1. We can create a new Blog: Create
2. See the posted blogs: Retrieve
3. Update a Blog: Update
4. Delete a Blog: Delete
5. The definitions of CRUD are summarized below:

###  The definitions of CRUD are summarized below:

If you’ve ever worked with a database, you’ve likely worked with CRUD operations. CRUD operations are often used with SQL, a topic we’ve covered in depth (see this article, this one, and this one for some of our recent SQL tips and tricks). Since SQL is pretty prominent in the development community, it’s crucial for developers to understand how CRUD operations work. So, this article is meant to bring you up to speed (if you’re not already) on CRUD operations.

## Working example:/
##### Html form:
![alt text](https://xp.io/storage/1C4XUHyS.png)

##### Adding Random data To form:
![alt text](https://xp.io/storage/1C55k0Br.png)
##### Submit Button _Post data to db_ 
![alt text](https://xp.io/storage/1C5esqAu.png)

##### End of the application
**See you in the next article !!**








